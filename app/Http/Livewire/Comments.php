<?php

namespace App\Http\Livewire;

use App\Models\Comment;
use App\Models\Ticket;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;

class Comments extends Component
{
    use WithPagination;


    public $image;

    public $newComments;

    public $ticketId;

    protected $listeners = [
        'fileUpload' => 'handleFileUpload',
        'ticketSelected',
    ];

    public function ticketSelected($ticketId){
        $this->ticketId = $ticketId;
    }

    public function updated($newComments){
        $this->validateOnly($newComments, [
            'newComments' => 'required|min:6|max:255',
            'image' => 'image|max:1024'
        ]);
    }

    public function handleFileUpload($image)
    {
        $this->image = $image;
    }

    public function addComments(){
        $this->validate([
            'newComments' => 'required|min:6|max:255',

        ]);

        $image = $this->storeImage();

        $createdComments = Comment::create([
            'body'  => $this->newComments,
            'user_id' => 1,
            'image'  => $image,
            'ticket_id' => $this->ticketId
        ]);

        $this->newComments = '';
        $this->image = '';

        session()->flash('message', 'Comment added successfully. ');

    }

    public function storeImage(){
        if(!$this->image){
            return null;
        }else{

            $image = $this->image;
            $img_Extension = Str::between($image, 'data:image/', ';base64,');
            $image_name = uniqid().'.'.$img_Extension;
            $location = public_path().'/images/';
            Image::make($image)->save($location.$image_name);
        }
        return $image_name;
    }


    public function render()
    {
        return view('livewire.comments',[
            'comments' => Comment::where('ticket_id',$this->ticketId)->latest()->paginate(2),
        ]);
    }



    public function remove($id){
        $comment = Comment::find($id);

        if($comment->image){
           $imgPath =  public_path().'/images/';
           $img = $imgPath.$comment->image;
           if(file_exists($img)){
               @unlink($img);
           }
        }
        $comment->delete();
        session()->flash('message', 'Comment deleted successfully. ');
    }
}

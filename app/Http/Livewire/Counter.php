<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Counter extends Component
{
    public $counter = 1;

    public function incremrent(){
        $this->counter++;
    }

    public function decriment(){
        $this->counter--;
    }

    public function render()
    {
        return view('livewire.counter');
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{
    public $form = [

        'email' => '',
        'password' => ''

    ];

    public function updated($form)
    {
        $this->validateOnly($form, [
            'form.password' => 'required',
            'form.email' => 'required|email',
        ]);
    }

    public function login(){
        $this->validate([
            'form.password' => 'required',
            'form.email' => 'required|email',
        ]);

        Auth::attempt($this->form);
        return redirect(route('home'));
    }

    public function render()
    {
        return view('livewire.login');
    }

    public function logout(){
        Auth::logout();
        return redirect(route('login'));
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Image as ModelsImage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;

class Image extends Component{

    use WithFileUploads;
    public $photos = [];

    public function render(){
        return view('livewire.image');
    }

    public function updatedPhoto(){
        $this->validate([
            'photos.*' => 'required|mimes:jpeg,png,svg,jpg,gif|image|max:1024', // 1MB Max
        ]);
    }

    public function save(){
        $this->validate([
            'photos.*' => 'required|image|mimes:jpeg,png,svg,jpg,gif|max:1024', // 1MB Max
        ]);

        foreach ($this->photos as $photo) {
            $image = $photo->storePublicly("photos");
            $imageName = Str::after($image , 'photos/');

            $uploadImage = ModelsImage::create([
                'image' => $imageName
            ]);
        }


        session()->flash('message', 'Image Uploaded successfully. ');
        $this->photos = [];
    }

    public function remove($index)
    {
        array_splice($this->photos, $index, 1);
    }
}

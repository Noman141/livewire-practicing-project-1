<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;

class Posts extends Component
{
    public function render()
    {
        return view('livewire.backend.posts');
    }
}

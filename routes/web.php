<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', \App\Http\Livewire\Home::class)->name('home')->middleware('auth');

Route::get('/image', \App\Http\Livewire\Image::class)->name('image')->middleware('auth');

Route::get('/login', \App\Http\Livewire\Login::class)->name('login')->middleware('guest');

Route::get('/register', \App\Http\Livewire\Register::class)->name('register')->middleware('guest');

Route::get('/logout', [\App\Http\Livewire\Login::class, 'logout'])->name('logout')->middleware('auth');

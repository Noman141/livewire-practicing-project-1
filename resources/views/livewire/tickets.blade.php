<div>
    <h2 class="text-3xl">Tickets</h2>
    @foreach ($tickets as $ticket)
    <div wire:click.prevent="$emit('ticketSelected',{{ $ticket->id }})" class="border rounded shadow cursor-pointer p-3 my-2 {{ $active == $ticket->id ? 'bg-blue-500 text-white':'text-gray-600' }}">
        <div class="ml-4">
            <p class="mt-2">{{ $ticket->ticket }}</p>
        </div>
    </div>
    @endforeach

</div>


<div>
    <h1 class="text-3xl">Comments</h1>
    @error('newComments') <span class="text-red-500 text-sm error">{{ $message }}</span> @enderror
    @error('image') <span class="error text-red-500 text-sm">{{ $message }}</span> @enderror
    @if (session()->has('message'))
        <div class="bg-green-500 text-white rounded shadow-lg p-4">
            {{ session('message') }}
        </div>
    @endif
    <img src="{{ $image }}" width="200">
    <form class="my-4 flex flex-col" wire:submit.prevent='addComments' enctype="multipart/form-data">
        <div>
            <input type="file" id="image" wire:change="$emit('fileChoosen')">
        </div>
        <div class="py-3">
            <input class="w-full rounded border shadow p-2 mr-2 my-2" wire:model.debounce.500ms='newComments' placeholder='whats in your mind'>
        </div>
        <div class="py-2">
            <button type="submit" class="bg-blue-500 text-white p-2 rounded w-20 shadow">Add</button>
        </div>
    </form>
    @foreach ($comments as $comment)
    <div class="border rounded shadow p-3 my-2">
        <div class="flex">
            @if($comment->image)
            <div>
                <img src= "{{ asset('images/'.$comment->image) }}" style="max-width: 200px">
            </div>
            @endif
            <div class="ml-4">
                <div class="flex">
                    <h3 class="font-bold text-lg">{{ $comment->user->name }}</h3>
                    <span class="mx-3 py-1 text-xs text-gray-500 font-semibold">{{ $comment->created_at->diffForHumans()}}</span>
                    <span class="text-red-400 hover:text-red-600 cursor-pointer ml-auto"><i class="fas fa-times" wire:click.prevent="remove({{ $comment->id }})"></i></span>
                </div>
                <p class="text-gray-600 mt-2">{{ $comment->body }}</p>
            </div>
        </div>
    </div>
    @endforeach

    <div class="mb-48">
        {{ $comments->links('vendor.livewire.tailwind') }}
    </div>
</div>
<script>
    Livewire.on('fileChoosen', () => {
        let commentImage = document.getElementById('image');
        let file = commentImage.files[0];
        let reader = new FileReader();
        reader.onload = () => {
            Livewire.emit('fileUpload', reader.result)
        };

        reader.readAsDataURL(file);
    })
</script>

<div class="my-10 flex justify-center w-full">
    <section class="border rounded shadow-lg p-4 w-6/12 bg-gray-200">
        <h1 class="text-center text-3xl my-5">SignUp To Get Start</h1>
        <hr>
        <form class="my-5" wire:submit.prevent='submit'>
            <div class="flex justify-around my-8">
                <div class="flex flex-wrap w-10/12">
                    <input type="text" wire:model="form.name" class="p-2 rounded border shadow-lg w-full" placeholder="Name">
                    @error('form.name') <span class="text-red-500 text-sm error">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="flex justify-around my-8">
                <div class="flex flex-wrap w-10/12">
                    <input type="email" wire:model="form.email" class="p-2 rounded border shadow-lg w-full" placeholder="Email">
                    @error('form.email') <span class="text-red-500 text-sm error">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="flex justify-around my-8">
                <div class="flex flex-wrap w-10/12">
                    <input type="password" wire:model="form.password" class="p-2 rounded border shadow-lg w-full" placeholder="Password">
                    @error('form.password') <span class="text-red-500 text-sm error">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="flex justify-around my-8">
                <div class="flex flex-wrap w-10/12">
                    <input type="password" wire:model="form.password_confirmation" class="p-2 rounded border shadow-lg w-full" placeholder="Confirm Password">
                </div>
            </div>

            <div class="flex justify-around my-8">
                <div class="flex flex-wrap w-10/12">
                    <input type="submit" value="Register" class="p-2 bg-gray-800 text-white w-full rounded tracking-wider cursor-pointer" placeholder="Confirm Password">
                </div>
            </div>
        </form>
    </section>
</div>

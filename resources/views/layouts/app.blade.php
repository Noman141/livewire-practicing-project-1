<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel Liveware</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">

    @livewireStyles

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/gh/livewire/turbolinks@v0.1.x/dist/livewire-turbolinks.js" data-turbolinks-eval="false"></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.3/dist/alpine.min.js" defer></script>

    <script src="{{ asset('js/app.js')}}"></script>
</head>
<body class="flex flex-wrap justify-center">
    <div class="flex w-full justify-left px-4 bg-purple-900 text-white">
        <div class="w-10/12 flex justify-between py-4 mx-auto">
            <a class="mx-3 py-4" href="{{ route('home')}}">Home</a>
            @auth
            <div>
                <a class="mx-3 py-4 cursor-pointer" href="{{ route('image')}}">Image</a>
                <a class="mx-3 py-4 cursor-pointer" href="{{ route('logout')}}">Logout</a>
            </div>
            @endauth
            @guest
            <div>
                <a class="mx-3" href="{{ route('login')}}">Login</a>
                <a class="mx-3" href="{{ route('register')}}">Register</a>
            </div>
            @endguest
        </div>
    </div>

    <div class=" my-10 w-full">
        {{ $slot }}
    </div>

</body>
</html>

